package capitalizer

import (
	"strings"

	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

func ToUpper(s string) string {
	return strings.ToUpper(s)
}

func Capitalize(s string) string {
	return cases.Title(language.English).String(s)
}
